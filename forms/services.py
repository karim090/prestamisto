import requests



def pre_score(dni):
    #URL = "http://nicanor.lan/test.html"
    URL = "https://api.moni.com.ar/api/v4/scoring/pre-score"
    headers = {'credential': 'ZGpzOTAzaWZuc2Zpb25kZnNubm5u'}
    response = requests.get(URL + "/" + dni, headers=headers)
    if response.status_code == 200:
        response_json = response.json()
        #print("api rta: %s" % response_json)
        has_error = bool(response_json.get("has_error"))
        status = str(response_json.get("status"))

    return status == "approve" and not has_error
    #return True
