from django.contrib import admin
from django.urls import path
from . import views

app_name = 'forms'
urlpatterns = [
    path('', views.index, name='index'),
    path('mi_prestamo', views.mi_prestamo, name='mi_prestamo'),
    path('prestamos_admin', views.admin, name='admin'),
    path('registrar_admin', views.registrar_admin, name='registrar_admin'),
    path('salir_admin', views.salir_admin, name='salir_admin'),


    path('cambiar_estado/<int:id>', views.cambiar_estado, name='cambiar_estado'),
    path('borrar/<int:id>', views.borrar, name='borrar'),

]