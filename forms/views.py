from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from email_validator import validate_email

from . import services
from .forms import RegistrarPrestamo
from .models import Prestamos



def index(request):
    return render(request,'index.html')


def mi_prestamo(request):
    if request.method == "POST":
        form = RegistrarPrestamo(request.POST)
        email = request.POST.get("email", "")
        if ( form.is_valid()):
            dni = request.POST.get("dni", "")
            nombre = request.POST.get("nombre", "")
            apellido = request.POST.get("apellido", "")
            genero = request.POST.get("genero", "")
            monto = request.POST.get("monto", "")
            try:
                solicitud = Prestamos.objects.get(dni=int(dni))
            except:
                solicitud = form
                solicitud.estado = services.pre_score(dni)
                solicitud.monto = monto
                solicitud.save()
            contexto = {
                "dni": dni,
                "nombre": nombre,
                "apellido": apellido,
                "genero": genero,
                "email": email,
                "monto": solicitud.monto,
                "resultado": solicitud.estado
            }
            solicitud.save()
            return render(request, "forms/mi_prestamo_resultado.html", {"contexto": contexto})
        else:
            return render(request, 'forms/mi_prestamo.html', {'register_form': form, "errores": ["email icorrecto"]})
    register_form = RegistrarPrestamo()
    return render(request, 'forms/mi_prestamo.html', {'register_form': register_form, "errores": []})


def admin(request):
    contexto = {}
    if request.user.is_authenticated:
        contexto["is_authenticated"] = request.user.is_authenticated
        contexto["prestamos"] = Prestamos.objects.all()
        return render(request, 'admin/prestamos_panel.html', {'contexto': contexto})
    else:
        form = AuthenticationForm()
        if request.method == "POST":
            form = AuthenticationForm(data=request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(username=username, password=password)
                if user is not None:
                    do_login(request, user)
                    return redirect('/prestamos_admin')

        return render(request, "admin/prestamos_panel.html", {'login_form': form})

def salir_admin(request):
    do_logout(request)
    return redirect('/')

def registrar_admin(request):
    form = UserCreationForm()
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            if user is not None:
                do_login(request, user)
                return redirect('/prestamos_admin')

    return render(request, "admin/registrar.html", {'form': form})


def cambiar_estado(request,id):
    try:
        if(id>0):
            prestamo = Prestamos.objects.get(id=int(id))
            prestamo.estado = not prestamo.estado
            prestamo.save()
    except:
        pass
    return HttpResponseRedirect("/prestamos_admin")


def borrar(request,id):
    try:
        if(id>0):
            prestamo = Prestamos.objects.get(id=int(id))
            prestamo.delete()
    except:
        pass
    return HttpResponseRedirect("/prestamos_admin")

