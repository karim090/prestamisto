from django import forms

from forms.constants import GENERO
from forms.models import Prestamos


class RegistrarPrestamo(forms.ModelForm):
    class Meta:
        model = Prestamos
        fields = [
            "dni",
            "nombre",
            "apellido",
            "genero",
            "email",
            "monto",
        ]
        labels = {
            "dni": "DNI",
            "nombre": "Nombre",
            "apellido": "Apellido",
            "genero": "Genero",
            "email": "E-mail",
            "monto": "Monto",
        }
        widgets = {
            "dni": forms.NumberInput(attrs={'min': 20000000, 'max': 99999999, 'class': "form-control"}),
            "nombre": forms.TextInput(attrs={'class': "form-control"}),
            "apellido": forms.TextInput(attrs={'class': "form-control"}),
            "genero": forms.Select(attrs={'class': "form-control"}),
            "email": forms.EmailInput(attrs={'class': "form-control"}),
            "monto": forms.NumberInput(attrs={'min': 1, 'max': 100000000000, 'class': "form-control"}),

        }
