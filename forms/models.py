from django.core.exceptions import ValidationError
from django.db import models
from django.core.validators import validate_email, RegexValidator
from email_validator import validate_email, EmailNotValidError
from forms.constants import GENERO


alnumValidator = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')



class Prestamos(models.Model):
    dni = models.IntegerField()
    nombre = models.CharField(max_length=20, validators=[alnumValidator])
    apellido = models.CharField(max_length=20, validators=[alnumValidator])
    genero = models.CharField(max_length=1, choices=GENERO)
    email = models.EmailField(max_length=254, validators=[validate_email])
    monto = models.IntegerField()
    estado = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre
